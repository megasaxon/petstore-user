<?php

namespace Petstore\test;

use Doctrine\ORM\EntityManager;
use Petstore\Controller\UserController;
use Petstore\Model\User;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class UserControllerTest extends TestCase
{

    protected $containerMock;

    protected $requestMock;

    protected $responseMock;

    protected $userMock;

    protected $entityManagerMock;

    public function setUp()
    {
        parent::setUp();

        $this->containerMock = $this->getMockBuilder(Container::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->responseMock = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityManagerMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
    }


    public function testCreateUserFromList()
    {

    }

    public function testUserHandler()
    {

    }

    public function testCreateUser()
    {
        $userController = new UserController($this->containerMock);

        $requestData = [
                'id'         => 999,
                'username'   => 'test',
                'firstName'  => 'FirstName',
                'lastName'   => 'LastName',
                'email'      => 'test@example.com',
                'password'   => 'password',
                'phone'      => '123456789',
                'userStatus' => 2
        ];

        $this->requestMock->expects($this->exactly(1))
            ->method('getBody')
            ->willReturn(json_encode($requestData));

        $this->containerMock->expects($this->exactly(1))
            ->method('get')
            ->with($this->equalTo(EntityManager::class))
            ->willReturn($this->entityManagerMock);

        $this->entityManagerMock->expects($this->exactly(1))
            ->method('persist');

        $this->responseMock->expects($this->exactly(1))
            ->method('getStatuscode')
            ->willReturn(999);

        $this->responseMock->expects($this->exactly(1))
            ->method('withStatus')
            ->willReturn($this->responseMock);

        $this->responseMock->expects($this->exactly(1))
            ->method('getReasonPhrase')
            ->willReturn('phrase');

        $this->responseMock->expects($this->exactly(1))
            ->method('withJson')
            ->willReturn($this->responseMock);

        $user1 = new User();
        $user1->setId($requestData['id']);
        $user1->setUsername($requestData['username']);
        $user1->setFirstName($requestData['firstName']);
        $user1->setLastName($requestData['lastName']);
        $user1->setEmail($requestData['email']);
        $user1->setPassword($requestData['password']);
        $user1->setPhone($requestData['phone']);
        $user1->setUserStatus($requestData['userStatus']);


        $user2 = $userController->createUser($this->requestMock, $this->responseMock);

        $this->assertEquals($user1, $user2);
    }

    public function testAuth()
    {
        $userController = new UserController($this->containerMock);

        //set up the mock object

        $responseData = [
            'code' => 501,
            'type' => 'testPhraes',
            'message' => 'SomeMesage'
        ];

        $this->responseMock->expects($this->exactly(1))
            ->method('getStatuscode')
            ->willReturn($responseData['code']);

        $this->responseMock->expects($this->exactly(1))
            ->method('withStatus')
            ->willReturn($this->responseMock);

        $this->responseMock->expects($this->exactly(1))
            ->method('getReasonPhrase')
            ->willReturn($responseData['type']);

        $this->responseMock->expects($this->exactly(1))
            ->method('withJson')
            ->willReturn($this->responseMock);

        $this->responseMock->expects($this->exactly(1))
            ->method('getBody')
            ->willReturn(json_encode($responseData));

        $authResult = $userController->auth($this->requestMock, $this->responseMock);

        $this->assertInstanceOf('Slim\Http\Response', $authResult);

        $this->assertEquals('501', $authResult->getStatusCode());

        $this->assertEquals(json_encode($responseData), $authResult->getBody());
    }
}
