# Petstore User

This package encompasses the petstore user service, and can be used to handle the user aspects of the api definition.

This would be deployed on a docker image independantly of the other petstore "services", such as pets, stores e.t.c.

### Setup
Please note that the following assumptions are made:

1. You have PHP version 7.3 installed
2. You have composer installed globally

#### Build Steps
Normally you would run `docker-composer up` however I've not finished that, so for now the following will demonstrate the application:

1. Run `composer install`
2. Run `vendor/bin/phing build`
2. Run `php -S localhost:8080 -t public/`

#### API Endpoints
All of the user endpoints are implemented, however the login/logout endpoints do nothing.

Hit the API with your choice of client, for example, hitting `http://localhost:8080/user/username1` would get information about the user with the username `username1`.




