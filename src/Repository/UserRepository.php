<?php
declare(strict_types=1);

namespace Petstore\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * Class UserRepository
 * @package Petstore\Repository
 */
class UserRepository extends EntityRepository
{

}
