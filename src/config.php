<?php
declare(strict_types=1);

use Symfony\Component\Dotenv\Dotenv;

define('APP_ROOT', __DIR__);
define('ROOT_PATH', APP_ROOT . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);


//read in .env file
$environmentData = new Dotenv();
$environmentData->load(ROOT_PATH . '.env');

return [
    'settings' => [
        'displayErrorDetails' => true,
        'determineRouteBeforeAppMiddleware' => true,
        'doctrine' => [
            'dev_mode' => true,
            'metadata_dirs' => [APP_ROOT . '/Model'],
            'connection' => [
                'driver' => getenv('DB_DRIVER'),
                'path' => ROOT_PATH . getenv('DB_DATABASE'),
            ]
        ]
    ]
];
