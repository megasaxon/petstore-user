<?php
declare(strict_types=1);

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Slim\Container;
use Petstore\Controller\UserController;

require_once __DIR__ . '/../vendor/autoload.php';

//create container, with config data
$container = new Container(require __DIR__ . '/config.php');

//persist an entityManager service
$container[EntityManager::class] = function (Container $container): EntityManager {

    $config = Setup::createAnnotationMetadataConfiguration(
        $container['settings']['doctrine']['metadata_dirs'],
        $container['settings']['doctrine']['dev_mode']
    );

    $config->setMetadataDriverImpl(
        new AnnotationDriver(
            new AnnotationReader(),
            $container['settings']['doctrine']['metadata_dirs']
        )
    );

    return EntityManager::create(
        $container['settings']['doctrine']['connection'],
        $config
    );
};

$container['UserController'] = function (Container $container) {
    return new UserController($container);
};

return $container;
