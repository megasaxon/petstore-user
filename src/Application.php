<?php
declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use Slim\App;

//init the container
$container = require_once __DIR__ . '/bootstrap.php';

//init the application
$app = new App($container);

//user methods
$app->group('/user/', function (App $app) {
    //handle create user route
    $app->post('', 'UserController:createUser');

    //handle create with list/array route
    $app->post('{type:[createWithList|createWithArray]+}', 'UserController:createUserFromList');

    //handle authentication
    $app->get('{auth:[login|logout]+}', 'UserController:auth');

    //handle retrevial/update/delete of users
    $app->map(['GET', 'PUT', 'DELETE'], '{username}', 'UserController:userHandler');
});


$app->run();
