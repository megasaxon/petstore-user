<?php
declare(strict_types=1);

namespace Petstore\Controller;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 * Base Controller
 * @package Petstore\Controller
 */
abstract class Controller
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Serializer
     */
    protected $serialiser;

    /**
     * Controller constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        //init the container
        $this->container = $container;

        //init the serialiser for easy object hydration
        $encoders = [ new JsonEncoder() ];
        $normalizers = [ new ObjectNormalizer(), new ArrayDenormalizer() ];

        $this->serialiser = new Serializer($normalizers, $encoders);
    }

    /**
     * Handle a response
     * @param ResponseInterface $response
     * @param int $code
     * @param string $message
     *
     * @return ResponseInterface
     */
    public function response(ResponseInterface $response, int $code = 200, string $message = '')
    {
        //take the response, and change the code
        $finalResponse = $response->withStatus($code);

        //create body for the response
        $responseBody = [
            'code' => $code,
            'type' => $finalResponse->getReasonPhrase(),
            'message' => $message
        ];

        //return the response with body as json
        return $finalResponse->withJson($responseBody);
    }

    /**
     * @param ResponseInterface $response
     * @param int $code
     * @param string $jsonData
     * @return ResponseInterface
     */
    public function jsonResponse(ResponseInterface $response, int $code = 200, string $jsonData = '{}')
    {
        //take the response, and change the code, and add the json content type header
        $finalResponse = $response->withStatus($code)->withHeader('Content-Type', 'application/json;charset=utf-8');

        //get the body, and write the raw JSON
        $body = $finalResponse->getBody();
        $body->write($jsonData);

        //return response
        return $finalResponse;
    }
}
