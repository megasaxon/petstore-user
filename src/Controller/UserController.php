<?php
declare(strict_types=1);

namespace Petstore\Controller;

use Doctrine\ORM\EntityManager;
use Petstore\Model\User;
use Psr\Http\Message\{
    RequestInterface,
    ResponseInterface
};

/**
 * Class UserController
 * @package Petstore\Controller
 */
class UserController extends Controller
{
    /**
     * Action for creating a user
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function createUser(RequestInterface $request, ResponseInterface $response) : ResponseInterface
    {

        //grab the body of the request
        $body = $request->getBody();

        //try to see if we can hydrate into the user class
        try {
            $user = $this->serialiser->deserialize($body, User::class, 'json', ['ignored_attributes' => ['id']]);
            //@todo: note there's no validation here at all, and there should be.
        } catch (\Exception $exception) {
            return $this->response($response, 400, $exception->getMessage());
        }

        //attempt to save to the database
        try {
            //get the entity manager
            $entityManager = $this->container->get(EntityManager::class);

            $entityManager->persist($user);
            $entityManager->flush();

            if ($user->getId()) {
                return $this->response($response, 201, 'User Created');
            } else {
                return $this->response($response, 500, 'Unable to save user to database');
            }
        } catch (\Exception $exception) {
            return $this->response($response, 500, $exception->getMessage());
        }
    }

    /**
     * Action for creating users from a list
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function createUserFromList(RequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        $body = $request->getBody();

        //try to see if we can hydrate into a collection of users
        try {
            $users = $this->serialiser->deserialize(
                $body,
                'Petstore\Model\User[]',
                'json',
                ['ignored_attributes' => ['id']]
            );
            //@todo: note there's no validation here at all, and there should be.
        } catch (\Exception $exception) {
            return $this->response($response, 400, $exception->getMessage());
        }

        //attempt to save to the database
        try {
            //get the entity manager
            $entityManager = $this->container->get(EntityManager::class);

            //loop through all of the specified users, and persist them
            foreach ($users as $user) {
                $entityManager->persist($user);
            }

            //now flush so they're stored.
            $entityManager->flush();

            //return success
            return $this->response($response, 201, 'successful operation');
        } catch (\Exception $exception) {
            //otherwise something went wrong
            return $this->response($response, 500, $exception->getMessage());
        }
    }

    /**
     * Handle updates, retrieval and deletion of users
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $arguments
     *
     * @return ResponseInterface
     */
    public function userHandler(
        RequestInterface $request,
        ResponseInterface $response,
        array $arguments
    ) : ResponseInterface {
        //attempt to load the user from the database
        $entityManager = $this->container->get(EntityManager::class);
        $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $arguments['username']]);

        if (!$user) {
            return $this->response($response, 404, 'User not found');
        }

        //handle retrieval of users
        if ($request->isGet()) {
            //serialise the user
            $userJson = $this->serialiser->serialize($user, 'json');
            return $this->jsonResponse($response, 200, $userJson);
        }

        //handle deletion of user
        if ($request->isDelete()) {
            $entityManager->remove($user);
            $entityManager->flush();

            return $this->response($response, 200, 'User deleted');
        }

        //handle user updates
        if ($request->isPut()) {
            // get request body
            $body = $request->getBody();

            try {
                //take body JSON and hydrate the user object
                $this->serialiser->deserialize(
                    $body,
                    User::class,
                    'json',
                    [
                        'ignored_attributes' => ['id'],
                        'object_to_populate' => $user
                    ]
                );
            } catch (\Exception $exception) {
                return $this->response($response, 400, $exception->getMessage());
            }

            try {
                //get the entity manager
                $entityManager = $this->container->get(EntityManager::class);

                //save our changes to the DB
                $entityManager->persist($user);
                $entityManager->flush();

                //this check is somewhat redundant...as it doesn't verify that the user was updated
                if ($user->getId()) {
                    return $this->response($response, 200, 'User Updated');
                } else {
                    return $this->response($response, 500, 'Unable to save user to database');
                }
            } catch (\Exception $exception) {
                return $this->response($response, 500, $exception->getMessage());
            }
        }

        //failsafe that just returns the username, if we dont hit one of the above blocks.
        return $this->response($response, 200, $arguments['username']);
    }
    /**
     * Auth handler...not implemented
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function auth(RequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        return $this->response($response, 501, 'Not Implemented');
    }
}
